from django import forms
from .models import form_model

class form(forms.ModelForm):
    class Meta:
        model = form_model
        fields = "__all__"