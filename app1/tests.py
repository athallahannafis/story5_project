from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve

# selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

from .models import form_model
from .forms import form
from .views import form_view, profile


# Create your tests here.
class story5_test(TestCase):

    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    # form function exist
    # def test_form_function_caller_exist(self):
    #     found = resolve('/form/')
    #     self.assertEqual(found.func, form_view)

    # profile function exist
    def test_landing_page_caller_exist(self):
        found = resolve("/")
        self.assertEqual(found.func, profile)

    def test_form_use_template(self):
        response = Client().get("/form/")
        self.assertTemplateUsed(response, "form.html")

    def test_model_can_create_new_status(self):
        form_model.objects.create(name="test", status="hanya test")
        count = form_model.objects.all().count()
        self.assertEqual(count, 1)

    def test_status_sukses(self):
        dic = {"name" : "test", "status" : "hanya test"}
        response = Client().post("/form/form_add/", dic, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(form_model.objects.all().count(), 1)

    def test_delete(self):
        form_model.objects.all().delete()
        count = form_model.objects.all().count()
        self.assertEqual(count, 0)
    
    # def test_delete_sukses(self):
    #     response = Client().post("/form/del_all")
    #     self.assertEqual(response.status_code, 200)
    #     self.assertEqual(form_model.objects.all().count(), 0)


class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome("./chromedriver", chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()
    
    def test_input(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000/form")
        # selenium.get(self.live_server_url)

        # find 
        name_input = selenium.find_element_by_id("id_name")
        status_input = selenium.find_element_by_id("id_status")
        status_submit = selenium.find_element_by_class_name("submit-button")
        time.sleep(3)

        # fill        
        name_input.send_keys("Test")
        status_input.send_keys("Coba bikin status")
        
        # submit
        status_submit.send_keys(Keys.RETURN)
        # selenium.get("http://127.0.0.1:8000/form")
        # self.assertIn("Test", browser.page_source)
        # self.assertIn("Coba bikin test", browser.page_source)