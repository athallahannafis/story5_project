from django.conf.urls import url
from .views import form_view, profile
from .views import form_add, del_all

urlpatterns = [
    url(r"^$", profile, name="profile"),
    url(r"^form/$", form_view, name="form"),
    url(r"^form/form_add/$", form_add, name="form_add"),
    url(r"^form/del_all/$", del_all, name="del_all")
]