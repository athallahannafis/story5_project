from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import form
from .models import form_model
# Create your views here.

def form_view(request):
    f = form(request.POST or None)
    # if request.method == "POST":
    #     if f.is_valid():
    #         f.save()
    # store all objects
    events = form_model.objects.all()
    return render(request, "form.html", {"form" : f, "events" : events})

def form_add(request):
    f = form(request.POST or None)
    if request.method == "POST":
        if f.is_valid():
            f.save()
    return HttpResponseRedirect("/form/", {"form" : f})

# def form_clear(request):
#     if request.method == "POST":
#         form_model.objects.all().delete()
#     return HttpResponseRedirect("/form/")
    

def profile(request):
    return render(request, "prof.html")

def del_all(request):
    if request.method == "POST":
        form_model.objects.all().delete()
    return HttpResponseRedirect("/form/")
